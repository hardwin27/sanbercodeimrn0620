//Soal 1
function teriak() {
    return "Halo Sanbers!"
}

console.log(teriak())

//Soal 2
function kalikan(angka1, angka2) {
    return angka1 * angka2
}

let num1 = 12
let num2 = 4
 
let hasilKali = kalikan(num1, num2)
console.log(hasilKali)

//Soal 3
function introduce(nama, umur, alamat, hobi) {
    return `Nama saya ${nama}, umur saya ${umur} tahun, alamat saya di ${alamat}, dan saya punya hobby yaitu ${hobi}`
}

let name = "Agus"
let age = 30
let address = "Jln. Malioboro, Yogyakarta"
let hobby = "Gaming"
 
let perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)