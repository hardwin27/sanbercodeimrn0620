import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Tugas12 from './Tugas/Tugas12/App.js';
import Tugas13 from './Tugas/Tugas13/App.js';
import Tugas14 from './Tugas/Tugas14/App.js';
import Tugas15 from './Tugas/Tugas15/index.js';
import Quiz3 from './Quiz3/index.js';


export default class App extends Component {
    render() {
        return (
            // <Tugas12/>
            // <Tugas13/>
            // <Tugas14/>
            // <Tugas15/>
            <Quiz3/>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
