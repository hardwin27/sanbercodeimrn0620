import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';

import HomeScreen from './components/HomeScreen.js';
import LoginScreen from './components/LoginScreen.js';
import RegisterScreen from './components/RegisterScreen.js';

class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                {/* <RegisterScreen/> */}
                {/* <LoginScreen/> */}
                <HomeScreen/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;