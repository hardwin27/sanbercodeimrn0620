import React, {Component} from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image
                    source={require('../images/logo.png')}
                    style={styles.logoImage}
                />
                <Text style={styles.title}>REGISTER</Text>
                <View style={styles.inputComponent}>
                    <Text style={styles.inputText}>Username</Text>
                    <TextInput style={styles.inputTextInput}/>
                </View>
                <View style={styles.inputComponent}>
                    <Text style={styles.inputText}>Email</Text>
                    <TextInput style={styles.inputTextInput}/>
                </View>
                <View style={styles.inputComponent}>
                    <Text style={styles.inputText}>Password</Text>
                    <TextInput style={styles.inputTextInput}/>
                </View>
                <View style={styles.inputComponent}>
                    <Text style={styles.inputText}>Ulangi Password</Text>
                    <TextInput style={styles.inputTextInput}/>
                </View>
                <View style={styles.buttonSection}>
                    <TouchableOpacity style={[styles.button, {backgroundColor: '#003366'}]}>
                        <Text style={styles.buttonText}>Daftar</Text>
                    </TouchableOpacity>
                    <Text style={{fontSize: 14, color: '#3EC6FF'}}>atau</Text>
                    <TouchableOpacity style={[styles.button, {backgroundColor: '#3EC6FF'}]}>
                        <Text style={styles.buttonText}>Masuk</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    logoImage: {
        marginTop: 25,
        marginBottom: 25
    },
    title: {
        fontSize: 25,
        textAlign: 'center',
        margin: 20,
        color: '#003366'
    },
    inputComponent: {
        marginTop: 10,
        marginBottom: 10
    },
    inputText: {
        fontSize: 14,
        color: '#3EC6FF'
    },
    inputTextInput: {
        borderWidth: 1,
        borderColor: '#3EC6FF',
        marginLeft: 10,
        marginRight: 10
    },
    buttonSection: {
        alignItems: 'center',
        marginVertical: 10
    },
    button: {
        borderRadius: 5,
        marginVertical: 5
    },
    buttonText: {
        fontSize: 16,
        color: '#fff',
        margin: 10
    }

});
