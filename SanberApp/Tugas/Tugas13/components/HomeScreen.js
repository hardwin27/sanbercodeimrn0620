import React, {Component} from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={[styles.text, {fontSize: 40, textAlign: 'center'}]}>Tentang Saya</Text>
                <MaterialCommunityIcons style={styles.profilePic} name='account-circle' size={100}/>
                <Text style={[styles.text, {fontSize: 20, textAlign: 'center'}]}>Hardwin Welly</Text>
                <Text style={[styles.text, {fontSize: 14, textAlign: 'center', color: '#3EC6FF'}]}>React Native Developer</Text>
                <View style={styles.itemBox}>
                    <View style={styles.underline}>
                        <Text>Portofolio</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                        <View style={styles.portfolio}>
                            <MaterialCommunityIcons name='gitlab' size={40}/>
                            <Text style={styles.text}>@hardwin27</Text>
                        </View>
                        <View style={styles.portfolio}>
                            <Ionicons name='logo-github' size={40}/>
                            <Text style={styles.text}>@hardwin27</Text>
                        </View>
                    </View>
                        
                </View>
                <View style={styles.itemBox}>
                    <View style={styles.underline}>
                        <Text>Hubungi Saya</Text>
                    </View>
                    <View style={styles.contactInfo}>
                        <MaterialCommunityIcons name='facebook' size={40}/>
                        <Text style={styles.text}>hardwin27</Text>
                    </View>
                    <View style={styles.contactInfo}>
                        <MaterialCommunityIcons name='instagram' size={40}/>
                        <Text style={styles.text}>@hardwin27</Text>
                    </View>
                    <View style={styles.contactInfo}>
                        <MaterialCommunityIcons name='twitter' size={40}/>
                        <Text style={styles.text}>@hardwin27</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginVertical: 25
    },
    text: {
        color: '#003366',
        fontSize: 14
    },
    profilePic: {
        alignSelf: 'center'
    },
    itemBox: {
        backgroundColor: '#EFEFEF',
        marginVertical: 15,
        borderRadius: 10
    },
    underline: {
        borderBottomWidth: 1,
        marginBottom: 30
    },
    portfolio: {
        flexDirection: 'column',
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    contactInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 5
    }
});