import React, {Component} from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    FlatList,
    Image
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SkillData from './skillData.json';

class SkillBox extends Component {
    render() {
        return(
            <View>
                <MaterialCommunityIcons name={this.props.iconName}/>
                <View>
                    <Text>{this.props.skillName}</Text>
                    <Text>{this.props.category}</Text>
                    <Text>{this.props.percentageProgress}</Text>
                </View>
                <MaterialCommunityIcons name='chevron-right'/>
            </View>
        )
    }
}

export default class App extends Component {
    render() {
        return(
            <View style={styles.container}>
                <View>
                    <Image source={require('./images/logo.png')}/>
                    <View style={{flexDirection: 'row'}}>
                        <MaterialCommunityIcons name='account-circle'/>
                        <View style={{flexDirection: 'column'}}>
                            <Text>Hi</Text>
                            <Text>Hardwin Welly</Text>
                        </View>
                    </View>
                    <View>
                        <Text>SKILL</Text>
                    </View>
                </View>
                <View>
                    <View>
                        <Text>Library/Framework</Text>
                    </View>
                    <View>
                        <Text>Bahasa Pemograman</Text>
                    </View>
                    <View>
                        <Text>Teknologi</Text>
                    </View>
                </View>
                <FlatList
                    data={SkillData["items"]}
                    renderItem={({item}) => <SkillBox
                        iconName={item.iconName}
                        skillName={item.skillName}
                        category={item.category}
                        percentageProgress={item.percentageProgress}
                    />}
                    keyExtractor={item => item.id}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        margin: 25
    }
});