//Soal 1
let index = 0;

while(index < 20){
    index += 2;
    console.log(index + " - I love coding");
}

while(index >= 2) {
    console.log(index + " - I will become a mobile developer");
    index -= 2;
}

//Soal 2
for(let number = 1; number <= 20; number++) {
    if(number % 2 == 0) {
        console.log(number + " - Berkualitas");
    }
    else {
        if(number % 3 == 0) {
            console.log(number + " - I Love Coding");
        }
        else {
            console.log(number + " - Santai");
        }
    }
}

//Soal 3
let dimXInit = 8;
let dimYInit = 4;
let dimX = 0;
let dimY = 0;
let hashSign = "";

dimY = dimYInit;
while(--dimY >= 0) {
    dimX = dimXInit;
    while(--dimX >= 0) {
        hashSign += "#";
    }
    console.log(hashSign);
    hashSign = "";
}

//Soal 4
let triangleSize = 7;
let hashForTraingle = "";

for(let x = 0; x < triangleSize; x++) {
    for(let y = 0; y <= x; y++) {
        hashForTraingle += "#";
    }
    console.log(hashForTraingle);
    hashForTraingle = "";
}

//Soal 5
let chessBoardDim = 8;
let chessBoardLine = "";

for(let x = 0; x < chessBoardDim; x++) {
    for(let y = 0; y < chessBoardDim; y++) {
        if(x % 2 == 0) {
            if(y % 2 == 0) {
                chessBoardLine += " ";
            }
            else {
                chessBoardLine += "#";
            }
        }
        else {
            if(y % 2 == 0) {
                chessBoardLine += "#";
            }
            else {
                chessBoardLine += " ";
            }
        }
    }
    console.log(chessBoardLine);
    chessBoardLine = "";
}