//Soal 2
let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function startReadBook(time, index) {
    readBooksPromise(time, books[index]).then(function(timeLeft) {
        startReadBook(timeLeft, index + 1);
    }).catch(function(timeLeft) {
        return;
    });
}

startReadBook(10000, 0);