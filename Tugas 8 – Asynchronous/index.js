//Soal 1
// di index.js
let readBooks = require('./callback.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini

function startReading(time, index) {
    readBooks(time, books[index], function(timeLeft) {
        if(index < 2) {
            index++;
            startReading(timeLeft, index);
        }
    })
}

startReading(10000, 0);