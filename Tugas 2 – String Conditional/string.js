//Soal 1
let word = 'JavaScript'; 
let second = 'is'; 
let third = 'awesome'; 
let fourth = 'and'; 
let fifth = 'I'; 
let sixth = 'love'; 
let seventh = 'it!';

console.log(word + " " + second + " " + third + " " + fourth + " " + fifth + " " + sixth + " " + seventh);

//Soal 2
let sentence = "I am going to be React Native Developer"; 

let exampleFirstWord = sentence[0] ; 
let exampleSecondWord = sentence[2] + sentence[3]  ; 
let thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]; // lakukan sendiri 
let fourthWord = sentence[11] + sentence[12]; // lakukan sendiri 
let fifthWord = sentence[14] + sentence[15]; // lakukan sendiri 
let sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21]; // lakukan sendiri 
let seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]; // lakukan sendiri 
let eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38]; // lakukan sendiri 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

//Soal 3
let sentence2 = 'wow JavaScript is so cool'; 

let exampleFirstWord2 = sentence2.substring(0, 3); 
let secondWord2 = sentence2.substring(4, 14); // do your own! 
let thirdWord2 = sentence2.substring(15, 17); // do your own! 
let fourthWord2 = sentence2.substring(18, 20); // do your own! 
let fifthWord2 = sentence2.substring(21, 25); // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

//Soal 4
let sentence3 = 'wow JavaScript is so cool'; 

let exampleFirstWord3 = sentence3.substring(0, 3); 
let secondWord3 = sentence3.substring(4, 14); // do your own! 
let thirdWord3 = sentence3.substring(15, 17); // do your own! 
let fourthWord3 = sentence3.substring(18, 20); // do your own! 
let fifthWord3 = sentence3.substring(21, 25); // do your own! 

let firstWordLength = exampleFirstWord3.length 
let secondWordLength = secondWord3.length;
let thirdWordLength = thirdWord3.length;
let fourthWordLength = fourthWord3.length;
let fifthWordLength = fifthWord3.length; 
// lanjutkan buat letiable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 