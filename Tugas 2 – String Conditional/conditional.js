//Soal 1
let nama = ""
let peran = ""

if(nama == "") {
    console.log("Nama harus diisi!");
}
else {
    if(peran == "") {
        console.log("Halo " + String(nama) + ", Pilih peranmu untuk memulai game!");
    }
    else {
        let pesan = ""
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        if(peran == "Penyihir") {
            pesan = ("kamu dapat melihat siapa yang menjadi werewolf!");
        }
        else if(peran == "Guard") {
            pesan = ("kamu akan membantu melindungi temanmu dari serangan werewolf");
        }
        else if(peran == "Werewolf") {
            pesan = ("Kamu akan memakan mangsa setiap malam!");
        }

        if(pesan != "") {
            console.log("Halo " + peran + " " + String(nama) + ", " + pesan);
        }
        else {
            console.log("Peran tidak tesedia. Peran yang ada: \nPenyihir\nGuard\nWerewolf");
        }
    }
}

//Soal 2
let hari = 16;
let bulan = 6;
let tahun = 2000;

let namaBulan = "";

switch(bulan) {
    case 1:
        namaBulan = "Januari";
        break;
    case 2:
        namaBulan = "Febuari";
        break;
    case 3:
        namaBulan = "Maret";
        break;
    case 4:
        namaBulan = "April";
        break;
    case 5:
        namaBulan = "Mei";
        break;
    case 6:
        namaBulan = "Juni";
        break;
    case 7:
        namaBulan = "Juli";
        break;
    case 8:
        namaBulan = "Agustus";
        break;
    case 9:
        namaBulan = "September";
        break;
    case 10:
        namaBulan = "Oktober";
        break;
    case 11:
        namaBulan = "November";
        break;
    case 12:
        namaBulan = "Desember";
        break;
}

if(!((hari > 0 && hari <= 31) && (bulan > 0 && bulan <= 12) && (tahun >= 1900 && tahun <= 2200))) {
    console.log("Salah satu input tidak valid. Silahkan coba lagi");
}
else {
    console.log(String(hari) + " " + namaBulan + " " + String(tahun));
}