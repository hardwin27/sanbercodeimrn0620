//Soal 1
function arrayToObject(arr) {
    // Code di sini 
    if(arr.length == 0) {
        console.log("");
    }
    else{
        let now = new Date()
        let thisYear = now.getFullYear()
        let tempObj = {};
        arr.forEach((value, index) => {
            tempObj = {
                firstName: value[0],
                lastName: value[1],
                gender: value[2],
                age: (typeof value[3] == "undefined" || value[3] > thisYear) ? "Invalid Birth Year" : thisYear -  value[3]
            };

            console.log(`${index + 1} . ${tempObj['firstName']} ${tempObj['lastName']} : `, tempObj);
        });
    }
    

}
 
// Driver Code
let people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people); 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
let people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2); 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]); // ""

//Soal 2
function shoppingTime(memberId, money) {
    // you can only write your code here!
    let purchasable = [
        {
            "name" : "Sepatu Stacattu",
            "price": 1500000
        },
        {
            "name" : "Baju Zoro",
            "price": 500000
        },
        {
            "name" : "Baju H&N",
            "price": 250000
        },
        {
            "name" : "Sweater Uniklooh",
            "price": 175000
        },
        {
            "name" : "Casing Handphone",
            "price": 50000
        }
    ];
    let purchased = [];
    let moneyAfterTransaction = 0;
    let tempObj = {};

    if(memberId == "" || memberId == null) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    else {
        if(money < 50000) {
            return "Mohon maaf, uang tidak cukup";
        }
        else {
            moneyAfterTransaction = money
            purchasable.forEach((value) => {
                if(moneyAfterTransaction >= value["price"]) {
                    purchased.push(value["name"]);
                    moneyAfterTransaction -= value["price"];
                }
            })
            tempObj = {
                memberId: memberId,
                money: money,
                listPurchased: purchased,
                changeMoney: moneyAfterTransaction
            };
            return tempObj;
        }
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  //Soal 3
  function naikAngkot(arrPenumpang) {
    let rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    let tarif = 2000;
    let tempObj = {}
    let arrTransaksi = []

    if(arrPenumpang.length == 0) {
        return arrPenumpang;
    }
    else {
        arrPenumpang.forEach((value) => {
            tempObj = {
                penumpang: value[0],
                naikDari: value[1],
                tujuan: value[2],
                bayar: (rute.indexOf(value[2]) - rute.indexOf(value[1])) * tarif
            }
            arrTransaksi.push(tempObj);
        })

        return arrTransaksi;
    }
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]